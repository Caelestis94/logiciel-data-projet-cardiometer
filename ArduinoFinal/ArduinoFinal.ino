#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include "RTClib.h"


RTC_DS1307 rtc;

const int chipSelect = 10;
const int analogPin = A0;
const int buttonPin = 7;
File dataFile;
String req = ""; // Request command 
void setup() {
  pinMode(buttonPin, INPUT);
  Serial.begin(9600);  
  while (!Serial) {
    ;
  }

  // Check if the RTC is connected and working
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  // Check if the RTC lost power and if so, set the time
  if (!rtc.isrunning()) {
    Serial.println("RTC lost power, let's set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  Serial.print("Initializing SD card...");
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    while (1);
  }
  Serial.println("card initialized.");
}
void wipeData() {
  if (SD.exists("datalog.txt")) {
    SD.remove("datalog.txt");
  }
}

bool lastButtonState = LOW;
bool isRecording = false;

void SaveTime()
{
  DateTime now = rtc.now();
  dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.println(now.unixtime());
    dataFile.close();
    // Dont print to the serial port..
  } else {
    Serial.println("error opening datalog.txt");
  }
  
}

void wasButtonPressed()
{
  bool buttonstate = digitalRead(buttonPin);
  if(buttonstate != lastButtonState)
  {
    lastButtonState = buttonstate;
    if(buttonstate == HIGH)
    {
      isRecording = !isRecording;
      SaveTime();
    }
  }
}

void loop() {
  while (Serial.available()){
    char c = Serial.read();
    if (c == '\n'){
      if (req == "DUMP") {
        dumpData();
      } else if (req == "WIPE"){
        wipeData();
      }
      req = "";
    } else {
      req += c;
    }
  }

  wasButtonPressed();

  if(isRecording){
    // make a string for assembling the data to log:
    String dataString = "";
    int sensor = analogRead(analogPin);
    dataString += String(sensor);
    delay(100);
    dataFile = SD.open("datalog.txt", FILE_WRITE);
    if (dataFile) {
      dataFile.println(dataString);
      dataFile.close();
      // Dont print to the serial port..
    } else {
      Serial.println("error opening datalog.txt");
    }
  }
}

void dumpData() {
  dataFile = SD.open("datalog.txt");
  if (dataFile) {
    while (dataFile.available()) {
      Serial.write(dataFile.read());
      Serial.write('$');
    }
    dataFile.close();
  } else {
   Serial.println("error opening datalog.txt");
  }
}
