﻿using CardiacometerDT;
using Newtonsoft.Json;
using MongoDB.Driver;
using System.IO.Ports;
using Formatting = Newtonsoft.Json.Formatting;
using System;

class Program
{
  static void Main(string[] args)
  {

    Console.WriteLine("begin");
    //SerialPort? port = null;
    //while(port == null)
    //{
    //  port = FindSerialPort();
    //}
    //List<string> allData = GetData(port);
    //ClearData(port);
    List<string> allData = fakeData.GetFakeDataEntrainements();
    Console.WriteLine("allData");
    List<string> dataEntrainement = fakeData.GetFakeDataEntrainements();
    int startIndex = 0;
    int index = 1;

    while(index < allData.Count)
    {
      Console.WriteLine(allData[index]);
      Console.WriteLine(index);
      if (allData[index].Length == 10)
      {
        Entrainement entrainement = CreateEntrainement(allData.GetRange(startIndex,index - startIndex+ 1));
        entrainement.ConvertRawToBPMs();
        SaveEntrainement(entrainement);
        index++;
        startIndex = index;
      }
      index++;
    }


  }

  public static Entrainement CreateEntrainement(List<string> dataEntrainement)
  {
    List<int> dataBPM = new List<int>();
    foreach (string s in dataEntrainement)
    {
      if (s.Length != 10)
      {
        dataBPM.Add(int.Parse(s));
      }
    }

    return new Entrainement(dataEntrainement[0], dataEntrainement[dataEntrainement.Count - 1], dataBPM);
  }

  public static void SaveEntrainementToJSON(Entrainement entrainement)
  {
    // Convert the object to a JSON string
    string jsonString = JsonConvert.SerializeObject(entrainement, Formatting.Indented);

    // Specify the file path
    string filePath = "entrainement.json";

    // Write the JSON string to a file
    File.WriteAllText(filePath, jsonString);
  }

  public static SerialPort FindSerialPort()
  {
    bool isError = true;
    int portNumber = 0;
    SerialPort port;
    while (isError && portNumber < 15)
    {
      string portName = String.Concat("COM", portNumber.ToString());
      port = new SerialPort(portName,9600);
      try
      {
        port.Open();
      }
      catch
      {
        isError = true;
        portNumber++; 
        port.Dispose();
        port = null;
        continue;
      }
      isError = false;
      return port;
    }
    return new SerialPort();
  }

  public static List<string> GetData(SerialPort port)
  {
    // Write the command to the serial port
    port.WriteLine("DUMP");

    // Delay to allow Arduino to process the command and start sending data
    System.Threading.Thread.Sleep(1000);

    List<string> list = new List<string>();

    while (list[list.Count() - 1] != "$")
    {
      list.Add(port.ReadLine());
      Console.WriteLine(list[list.Count()]);
    }
    return list;
  }

  public static void ClearData(SerialPort port)
  {
    // Write the command to the serial port
    port.WriteLine("WIPE");
  }

  public static void SaveEntrainement(Entrainement entrainement)
  {
    string connectionString = "mongodb+srv://OUEL99:veilletechno23!@cluster0.e5mchxl.mongodb.net/?retryWrites=true&w=majority";

    var client = new MongoClient(connectionString);

    var database = client.GetDatabase("cardiacometer");

    var collection = database.GetCollection<Entrainement>("entrainement");

    collection.InsertOne(entrainement);
  }
}