﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardiacometerDT
{
  public class Entrainement
  {
    public ObjectId? Id { get; set; }
    public string etudiantId { get; set; }
    public string DebutEntrainement { get; set; }
    public string FinEntrainement { set; get; }
    public List<BPM> BPMs { get; set; }
    List<int> RawBPM { get; set; }

    public Entrainement(string debut,string fin,List<int> rawBPM)
    {
      Id = ObjectId.GenerateNewId();
      etudiantId = "007";
      DebutEntrainement = debut;
      FinEntrainement = fin;
      BPMs = new List<BPM>();
      RawBPM = rawBPM;
    }

    public void ConvertRawToBPMs()
    {
      double avg = RawBPM.Average();
      List<int> indexPeak = new List<int>();
      int i = 1;
      while (i < RawBPM.Count - 1)
      {
        if(RawBPM[i] > avg && RawBPM[i] > RawBPM[i-1] && RawBPM[i] > RawBPM[i + 1])
        {
          indexPeak.Add(i);
          Console.WriteLine(i);
        }
        i++;
      }
      List<int> differencesPeakBPMs = new List<int>();
      BPM NextBPM = new BPM();
      double debut = Math.Round( indexPeak[0] * 0.1 + int.Parse(DebutEntrainement));
      NextBPM.DebutBPM = debut.ToString();
      i = 1;
      int nextBPMIndex = 1;
      while (i < indexPeak.Count)
      {
        differencesPeakBPMs.Add(indexPeak[i] - indexPeak[i-1]);
        if(Math.Round(indexPeak[i] / 60f) == nextBPMIndex)
        {
          double end = Math.Round(indexPeak[i] * 0.1 + int.Parse(DebutEntrainement));
          NextBPM.FinBPM = end.ToString();

          double avg_diff = differencesPeakBPMs.Average();
          NextBPM.BattementParMinute = avg_diff*10;

          BPMs.Add(NextBPM);
          NextBPM = new BPM();
          NextBPM.DebutBPM = end.ToString();
          nextBPMIndex++;
        }
        i++;
      }
      i--;
      double last_end = Math.Round(indexPeak[i] * 0.1 + int.Parse(DebutEntrainement));
      NextBPM.FinBPM = last_end.ToString();

      double last_avg_diff = differencesPeakBPMs.Average();
      NextBPM.BattementParMinute = last_avg_diff * 10;

      BPMs.Add(NextBPM);
    }
    
  }
}
