﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardiacometerDT
{
  public class BPM
  {
    public string DebutBPM { get; set; }
    public string FinBPM { set; get; }
    public double BattementParMinute { get; set; }
  }
}
